# Generated by Django 2.0.4 on 2018-04-16 15:26

from django.db import migrations
import home.blocks
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_homepage_navigation'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='contact_body',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock())), default={}),
            preserve_default=False,
        ),
    ]
