# Generated by Django 2.0.4 on 2018-04-20 19:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0024_homepage_event_page'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventpage',
            name='duration',
            field=models.DurationField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='eventpage',
            name='end',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
