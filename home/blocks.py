from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock


class HeadingBlock(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/heading.html'
        classname = 'heading_input struct-block'


class H2Block(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/h2.html'
        classname = 'heading2_input struct-block'


class H3Block(blocks.CharBlock):

    class Meta:
        icon = 'title'
        template = 'blocks/h3.html'
        classname = 'heading3_input struct-block'


class BlockQuoteBlock(blocks.StructBlock):

    text = blocks.RichTextBlock()
    credit = blocks.RichTextBlock(required=False)

    class Meta:
        icon = 'openquote'
        template = 'blocks/blockquote.html'
        classname = 'blockquote_input struct-block'


class IllustratedParagraphBlock(blocks.StructBlock):

    text = blocks.RichTextBlock()
    image = ImageChooserBlock()
    image_position = blocks.ChoiceBlock((
        ('left', 'Float left'),
        ('right', 'Float right'),
    ))

    class Meta:
        icon = 'doc-full'
        template = 'blocks/illustrated_paragraph.html'


class NavigationBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    anchor = blocks.CharBlock(required=False)
    page = blocks.PageChooserBlock(required=False)

    class Meta:
        icon = 'arrow-right'
        template = 'blocks/navigation.html'


class ContactInfoBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    name = blocks.CharBlock(required=False)
    url = blocks.URLBlock(required=False)

    class Meta:
        icon = 'link'
        template = 'blocks/contact_info.html'


class ContactIconBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=False)
    icon = blocks.CharBlock()
    url = blocks.URLBlock(required=False)

    class Meta:
        icon = 'image'
        template = 'blocks/contact_icon.html'


class ContactIconListBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    icons = blocks.ListBlock(ContactIconBlock())

    class Meta:
        icon = 'mail'
        template = 'blocks/contact_icon_list.html'
