wheel
django-debug-toolbar
Django
wagtail
psycopg2-binary
django-bootstrap3
# wagtail-markdown
git+https://github.com/vegaelle/wagtail-markdown@main#egg=wagtail-markdown
# django-wagtail-feeds
git+https://github.com/vegaelle/django-wagtail-feeds@fix_i18n_import#egg=django-wagtail-feeds
wagtail-localize
django-extensions
