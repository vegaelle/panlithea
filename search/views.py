from collections import namedtuple

from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render, reverse
from django.conf import settings
from django.utils.translation import get_language

from wagtail.core.models import Page, Locale
from wagtail.search.models import Query


def search(request):
    search_query = request.GET.get('query', None)
    page = request.GET.get('page', 1)

    # Search
    if search_query:
        locale = Locale.objects.get(language_code=get_language())
        root_page = request.site.root_page.get_translation(locale.id)
        search_results = Page.objects.live().descendant_of(root_page, inclusive=True)\
            .search(search_query)
        query = Query.get(search_query)

        # Record hit
        query.add_hit()
    else:
        search_results = Page.objects.none()

    # Pagination
    paginator = Paginator(search_results, settings.SEARCH_ITEMS_PER_PAGE)
    try:
        search_results = paginator.page(page)
    except PageNotAnInteger:
        search_results = paginator.page(1)
    except EmptyPage:
        search_results = paginator.page(paginator.num_pages)

    FakePage = namedtuple('FakePage', ['title', 'seo_title', 'path'])
    template_name = 'search/search.html' if request.site.is_default_site else \
        '{}/search.html'.format(request.site.root_page.content_type.app_label)
    return render(request, template_name, {
        'search_query': search_query,
        'search_results': search_results,
        'self': FakePage(title='Résultats de la recherche',
                         seo_title='Recherche',
                         path=reverse(search))
    })
