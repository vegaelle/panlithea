from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.contrib.table_block.blocks import TableBlock

from wagtailmarkdown.blocks import MarkdownBlock

from home.blocks import IllustratedParagraphBlock, BlockQuoteBlock


languages = (
    ('python', 'Python'),
    ('html', 'HTML'),
    ('css', 'CSS'),
    ('javascript', 'JavaScript'),
    ('sql', 'SQL'),
    ('bash', 'Bash'),
    ('django', 'Django template'),
    ('jinja', 'Jinja template'),
    ('diff', 'Diff'),
    ('xml', 'XML'),
    ('yaml', 'YAML'),
    ('json', 'JSON'),
)


class CodeBlock(blocks.StructBlock):

    text = blocks.TextBlock(classname='code_input')
    language = blocks.ChoiceBlock(choices=languages)

    class Meta:
        icon = 'code'
        template = 'blocks/code.html'
        classname = 'code_input struct-block'


class SubSectionBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    body = blocks.StreamBlock([
        ('paragraph', blocks.RichTextBlock(
            features=['bold', 'italic', 'h4', 'link', 'ol', 'ul',
                      'document-link', 'image', 'embed'])),
        ('markdown', MarkdownBlock(icon='code')),
        ('table', TableBlock()),
        ('quote', BlockQuoteBlock()),
        ('code', CodeBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock(template='blocks/image.html')),
        ('page', blocks.PageChooserBlock(template='blocks/page.html')),
        ('document', DocumentChooserBlock(template='blocks/document.html')),

    ])

    class Meta:
        icon = 'title'
        template = 'blocks/sub_section.html'


class SectionBlock(blocks.StructBlock):
    title = blocks.CharBlock()
    body = blocks.StreamBlock([
        ('sub_section', SubSectionBlock()),
        ('paragraph', blocks.RichTextBlock(
            features=['bold', 'italic', 'h4', 'link', 'ol', 'ul',
                      'document-link', 'image', 'embed'])),
        ('markdown', MarkdownBlock(icon='code')),
        ('table', TableBlock()),
        ('quote', BlockQuoteBlock()),
        ('code', CodeBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock(template='blocks/image.html')),
        ('page', blocks.PageChooserBlock(template='blocks/page.html')),
        ('document', DocumentChooserBlock(template='blocks/document.html')),
    ])

    class Meta:
        icon = 'title'
        template = 'blocks/section.html'
