from django.shortcuts import render
from django.contrib.auth.views import LoginView


def handler404(request, exception=None, template_name='404.html'):
    template_name = template_name if request.site.is_default_site else \
        '{}/404.html'.format(request.site.root_page.content_type.app_label)
    return render(request, template_name, {}, status=404)


def login(request):
    template_name = '{}/login.html'.format(
        request.site.root_page.content_type.app_label)
    return LoginView.as_view()(request, template_name=template_name)
