from django.conf import settings
from django.urls import include, re_path
from django.contrib import admin
from django.shortcuts import render
from django.conf.urls.i18n import i18n_patterns

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from wagtail_feeds.feeds import (
    BasicFeed, BasicJsonFeed, ExtendedFeed, ExtendedJsonFeed
)

from search import views as search_views
from .views import login

handler404 = 'panlithea.views.handler404'

urlpatterns = [
    re_path(r'^django-admin/', admin.site.urls),

    re_path(r'^admin/', include(wagtailadmin_urls)),
    re_path(r'^documents/', include(wagtaildocs_urls)),

]

urlpatterns = urlpatterns + i18n_patterns(
    re_path(r'^search/$', search_views.search, name='search'),
    re_path(r'^login/$', login, name='login'),

    re_path(r'^blog/feed/basic$', BasicFeed(), name='basic_feed'),
    re_path(r'^blog/feed/extended$', ExtendedFeed(), name='extended_feed'),

    # JSON feed
    re_path(r'^blog/feed/basic.json$', BasicJsonFeed(), name='basic_json_feed'),
    re_path(r'^blog/feed/extended.json$', ExtendedJsonFeed(),
        name='extended_json_feed'),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    re_path(r'', include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    re_path(r'^pages/', include(wagtail_urls)),
)


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

    from .views import handler404
    urlpatterns = [
        re_path(r'^404/?$', handler404),
        re_path(r'^500/?$', lambda r: render(r, '500.html', {})),
    ] + urlpatterns
