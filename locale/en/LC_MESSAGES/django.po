# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-19 10:42+0200\n"
"PO-Revision-Date: 2021-03-23 22:18+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#: docs/templates/blocks/section.html:7 docs/templates/docs/doc_page.html:40
msgid "Top"
msgstr "Top"

#: docs/templates/docs/404.html:13 panlithea/templates/404.html:10
msgid "Page not found"
msgstr "Page not found"

#: docs/templates/docs/404.html:20 panlithea/templates/404.html:12
msgid ""
"We’re sorry, but the page you’re looking for doesn’t exist. However, you can "
"search for your page here."
msgstr ""
"We’re sorry, but the page you’re looking for doesn’t exist. However, you can "
"search for your page here."

#: docs/templates/docs/404.html:28 docs/templates/docs/base.html:78
#: docs/templates/docs/search.html:20 panlithea/templates/404.html:20
#: panlithea/templates/base.html:61 search/templates/search/search.html:17
msgid "Search"
msgstr "Search"

#: docs/templates/docs/404.html:40 panlithea/templates/404.html:30
msgid "Image by <a href=\"https://http.cat/\">HTTP.cat</a>"
msgstr "Image by <a href=\"https://http.cat/\">HTTP.cat</a>"

#: docs/templates/docs/doc_page.html:22 docs/templates/docs/doc_page.html:39
msgid "Sub-pages"
msgstr "Sub-pages"

#: docs/templates/docs/search.html:13
msgid "Search results"
msgstr "Search results"

#: docs/templates/docs/search.html:41 search/templates/search/search.html:31
msgid "No results for this search query."
msgstr "No results for this search query."

#: docs/templates/docs/search.html:47 search/templates/search/search.html:33
msgid "Please enter a search query."
msgstr "Please enter a search query."

#: home/models.py:64
msgid "Content"
msgstr "Content"

#: home/models.py:65
msgid "Promote"
msgstr "Promote"

#: home/models.py:66
msgid "Settings"
msgstr "Settings"

#: home/templates/home/blog_index_page.html:23
#: home/templates/home/home_page.html:25
msgid "Read more"
msgstr "Read more"

#: home/templates/home/home_page.html:53
msgid "Send a message"
msgstr "Send a message"

#: home/templates/home/standard_page.html:14
msgid "Alternative download: "
msgstr "Alternative download: "

#: panlithea/templates/base.html:43
msgid "Continue"
msgstr "Continue"

#: panlithea/templates/wagtailadmin_/pages/create.html:5
#, python-format
msgid "New %(page_type)s"
msgstr "New %(page_type)s"

#: panlithea/templates/wagtailadmin_/pages/create.html:16
msgid "New"
msgstr "New"

#: panlithea/templates/wagtailadmin_/pages/create.html:32
#: panlithea/templates/wagtailadmin_/pages/create.html:46
#: panlithea/templates/wagtailadmin_/pages/edit.html:44
#: panlithea/templates/wagtailadmin_/pages/edit.html:77
msgid "Publishing…"
msgstr "Publishing…"

#: panlithea/templates/wagtailadmin_/pages/create.html:32
#: panlithea/templates/wagtailadmin_/pages/create.html:46
#: panlithea/templates/wagtailadmin_/pages/edit.html:44
#: panlithea/templates/wagtailadmin_/pages/edit.html:77
msgid "Publish"
msgstr "Publish"

#: panlithea/templates/wagtailadmin_/pages/create.html:36
#: panlithea/templates/wagtailadmin_/pages/create.html:41
#: panlithea/templates/wagtailadmin_/pages/edit.html:56
#: panlithea/templates/wagtailadmin_/pages/edit.html:64
msgid "Saving…"
msgstr "Saving…"

#: panlithea/templates/wagtailadmin_/pages/create.html:36
#: panlithea/templates/wagtailadmin_/pages/create.html:41
#: panlithea/templates/wagtailadmin_/pages/edit.html:56
#: panlithea/templates/wagtailadmin_/pages/edit.html:64
msgid "Save draft"
msgstr "Save draft"

#: panlithea/templates/wagtailadmin_/pages/create.html:38
#: panlithea/templates/wagtailadmin_/pages/create.html:49
#: panlithea/templates/wagtailadmin_/pages/edit.html:59
#: panlithea/templates/wagtailadmin_/pages/edit.html:81
msgid "Submit for moderation"
msgstr "Submit for moderation"

#: panlithea/templates/wagtailadmin_/pages/create.html:56
#: panlithea/templates/wagtailadmin_/pages/edit.html:90
msgid "Preview"
msgstr "Preview"

#: panlithea/templates/wagtailadmin_/pages/create.html:92
#: panlithea/templates/wagtailadmin_/pages/edit.html:137
msgid "This page has unsaved changes."
msgstr "This page has unsaved changes."

#: panlithea/templates/wagtailadmin_/pages/edit.html:6
#, python-format
msgid "Editing %(page_type)s: %(title)s"
msgstr "Editing %(page_type)s: %(title)s"

#: panlithea/templates/wagtailadmin_/pages/edit.html:17
#, python-format
msgid "Editing %(page_type)s <span>%(title)s</span>"
msgstr "Editing %(page_type)s <span>%(title)s</span>"

#: panlithea/templates/wagtailadmin_/pages/edit.html:20
msgid "Status"
msgstr "Status"

#: panlithea/templates/wagtailadmin_/pages/edit.html:44
#: panlithea/templates/wagtailadmin_/pages/edit.html:77
msgid "Publish this revision"
msgstr "Publish this revision"

#: panlithea/templates/wagtailadmin_/pages/edit.html:50
#: panlithea/templates/wagtailadmin_/pages/edit.html:70
msgid "Unpublish"
msgstr "Unpublish"

#: panlithea/templates/wagtailadmin_/pages/edit.html:53
#: panlithea/templates/wagtailadmin_/pages/edit.html:73
msgid "Delete"
msgstr "Delete"

#: panlithea/templates/wagtailadmin_/pages/edit.html:56
#: panlithea/templates/wagtailadmin_/pages/edit.html:64
msgid "Page locked"
msgstr "Page locked"

#: panlithea/templates/wagtailadmin_/pages/edit.html:56
#: panlithea/templates/wagtailadmin_/pages/edit.html:64
msgid "Replace current draft"
msgstr "Replace current draft"

#: panlithea/templates/wagtailadmin_/pages/edit.html:111
#, python-format
msgid "Last modified: %(last_mod)s"
msgstr "Last modified: %(last_mod)s"

#: panlithea/templates/wagtailadmin_/pages/edit.html:113
#, python-format
msgid "by %(modified_by)s"
msgstr "by %(modified_by)s"

#: panlithea/templates/wagtailadmin_/pages/edit.html:116
msgid "Revisions"
msgstr "Revisions"

#: search/templates/search/search.html:10
#, fuzzy
#| msgid "Search results"
msgid "Search results: "
msgstr "Search results"

#~ msgid "Visit the Wagtail website"
#~ msgstr "Visit the Wagtail website"

#~ msgid "View the release notes"
#~ msgstr "View the release notes"

#~ msgid "Welcome to your new Wagtail site!"
#~ msgstr "Welcome to your new Wagtail site!"

#~ msgid ""
#~ "Please feel free to <a href=\"https://github.com/wagtail/wagtail/wiki/"
#~ "Slack\">join our community on Slack</a>, or get started with one of the "
#~ "links below."
#~ msgstr ""
#~ "Please feel free to <a href=\"https://github.com/wagtail/wagtail/wiki/"
#~ "Slack\">join our community on Slack</a>, or get started with one of the "
#~ "links below."

#~ msgid "Wagtail Documentation"
#~ msgstr "Wagtail Documentation"

#~ msgid "Topics, references, & how-tos"
#~ msgstr "Topics, references, & how-tos"

#~ msgid "Tutorial"
#~ msgstr "Tutorial"

#~ msgid "Build your first Wagtail site"
#~ msgstr "Build your first Wagtail site"

#~ msgid "Admin Interface"
#~ msgstr "Admin Interface"

#~ msgid "Create your superuser first!"
#~ msgstr "Create your superuser first!"
